import '@babel/polyfill'
import './plugins/vuetify'
import './registerServiceWorker'
import '@mdi/font/css/materialdesignicons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

import firebase from 'firebase/app'
import Vue from 'vue'
import Vuelidate from 'vuelidate'

import router from './router'
import store from './store/store'

import App from './App.vue'
import eventBus from "./plugins/eventBus";

require('firebase/auth');
require('firebase/functions');

Vue.config.productionTip = false;

Vue.use(eventBus);
Vue.use(Vuelidate);

let app;

firebase.initializeApp(({
    apiKey: "AIzaSyCu73H_CEjAJMSfWIlMGsaS5QXX6sKoF4g",
    authDomain: "tcc-auth.firebaseapp.com",
    databaseURL: "https://tcc-auth.firebaseio.com",
    projectId: "tcc-auth",
    storageBucket: "tcc-auth.appspot.com",
    messagingSenderId: "831270593832",
    appId: "1:831270593832:web:2f144c4d128345ff"
}));

firebase.auth().onAuthStateChanged(user => {
    if (!app) {
        app = new Vue({
            router,
            store,
            render: h => h(App)
        }).$mount('#app');
    }
});
