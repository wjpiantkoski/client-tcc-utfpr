import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase/app'

import Index from './views/Index'
import Login from './views/Login'
import Auth from './views/Auth'
import Settings from './views/Settings'

Vue.use(Router);

let router = new Router({
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Settings,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/auth',
            name: 'Auth',
            component: Auth,
            beforeEnter: (to, from, next) => {
                if (!!firebase.auth().currentUser)
                    next('/');
                else
                    next();
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            beforeEnter: (to, from, next) => {
                if (!!firebase.auth().currentUser)
                    next('/');
                else
                    next();
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    let user = firebase.auth().currentUser;
    let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !user) {
        if (to.path === '/auth')
            next();
        else
            next('/auth');
    } else {
        next();
    }
});

export default router;
