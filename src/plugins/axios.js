import axios from 'axios'
import httpAdapter from 'axios/lib/adapters/http'

export default axios.create({
    baseURL: 'http://localhost:3000',
    adapter: httpAdapter
});
