import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import pt from 'vuetify/es5/locale/pt'

Vue.use(Vuetify, {
    theme: {
        primary: '#002a16',
        secondary: '#ada8a5',
        accent: '#05fb8e',
        error: '#cc3433',
        info: '#154692',
        success: '#198d38',
        warning: '#fad725'
    },
    iconfont: 'mdi',
    lang: {
        locales: {pt},
        current: 'pt'
    },
});
