import {unionBy} from 'lodash'

export default {
    setPartners: (state, payload) => {
        state.partners = unionBy(state.partners, payload, '_id');
    },
    pushPartner: (state, payload) => {
        state.partners.push(payload);
    },
    removePartners: (state, payload) => {
        payload.forEach(id => {
            let partner = state.partners.find(p => p._id === id);
            let index = state.partners.indexOf(partner);
            state.partners.splice(index, 1);
        });
    },
    updatePartner: (state, payload) => {
        let partner = state.partners.find(p => p._id === payload._id);
        let index = state.partners.indexOf(partner);
        state.partners.splice(index, 1, payload);
    }
}
