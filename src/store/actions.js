export default {
    setPartners: (context, payload) => {
        context.commit('setPartners', payload);
    },
    pushPartner: (context, payload) => {
        context.commit('pushPartner', payload);
    },
    removePartners: (context, payload) => {
        context.commit('removePartners', payload);
    },
    updatePartner: (context, payload) => {
        context.commit('updatePartner', payload);
    }
}
