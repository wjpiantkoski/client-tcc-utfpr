import firebase from 'firebase/app'

import router from '@/router'
import store from '@/store/store'

export default () => {
    firebase.auth().signOut()
        .then(() => {
            router.push('/auth');
            store.dispatch('setUser', null);
            store.dispatch('setPartners', null);
        });
}
