import firebase from 'firebase/app'
import axios from '@/plugins/axios'
import store from '@/store/store'
import logout from "../../logout";

export default data => new Promise((resolve, reject) => {
    firebase.auth().currentUser.getIdToken(true)
        .then(idToken => axios({
            method: 'post',
            url: '/partners/check-cnpj',
            headers: {
                Authorization: `Bearer ${idToken}`
            },
            data
        }))
        .then(resp => {
            if (resp.data.status) {
                resolve(resp.data.exists);
            } else {
                reject(resp.data);
            }
        })
        .catch(err => {
            if (process.env.NODE_ENV !== 'production') {
                console.error(err);
            }

            if (err.response.status === 401) {
                logout();
            } else {
                reject();
            }
        });
});
