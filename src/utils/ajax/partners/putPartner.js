import firebase from 'firebase/app'
import axios from '@/plugins/axios'
import store from '@/store/store'
import logout from "../../logout";

export default data => new Promise((resolve, reject) => {
    firebase.auth().currentUser.getIdToken(true)
        .then(idToken => axios({
            method: 'put',
            url: `/partners/${data._id}`,
            headers: {
                Authorization: `Bearer ${idToken}`
            },
            data
        }))
        .then(resp => {
            if (resp.data.status) {
                store.dispatch('updatePartner', resp.data.partner);
                resolve();
            } else {
                if (process.env.NODE_ENV !== 'production') {
                    console.error(resp.data);
                }
                reject();
            }
        })
        .catch(err => {
            if (process.env.NODE_ENV !== 'production') {
                console.error(err);
            }

            if (err.response.status === 401) {
                logout();
            } else {
                reject();
            }
        });
})
