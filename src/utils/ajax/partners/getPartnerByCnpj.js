import axios from '@/plugins/axios'

export default cnpj => new Promise((resolve, reject) => {
    axios.get(`/partners/${cnpj}`)
        .then(resp => {
            if (resp.data.status) {
                resolve(resp.data.partner);
            } else {
                if (process.env.NODE_ENV !== 'production') {
                    console.error(resp.data);
                }
                reject();
            }
        })
        .catch(err => {
            if (process.env.NODE_ENV !== 'production') {
                console.error(err);
            }
            reject();
        });
});
