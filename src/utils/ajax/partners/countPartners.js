import firebase from 'firebase/app'
import axios from '@/plugins/axios'
import store from '@/store/store'
import logout from "../../logout";

export default () => new Promise((resolve, reject) => {
    firebase.auth().currentUser.getIdToken(true)
        .then(idToken => axios({
            method: 'get',
            url: '/partners/count',
            headers: {
                Authorization: `Bearer ${idToken}`
            }
        }))
        .then(resp => {
            if (resp.data.status) {
                resolve(resp.data.count);
            } else {
                if (process.env.NODE_ENV !== 'production') {
                    console.error(resp.data);
                }
                reject();
            }
        })
        .catch(err => {
            if (process.env.NODE_ENV !== 'production') {
                console.error(err);
            }

            if (err.response.status === 401) {
                logout();
            } else {
                reject();
            }
        })
});
