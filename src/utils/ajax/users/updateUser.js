import firebase from 'firebase/app'

import router from '@/router'
import store from '@/store/store'
import axios from '@/plugins/axios'
import logout from "../../logout";

export default data => new Promise((resolve, reject) => {
    firebase.auth().currentUser.getIdToken(true)
        .then(idToken => axios({
            method: 'put',
            url: `/users/${data.uid}`,
            headers: { Authorization: `Bearer ${idToken}` },
            data
        }))
        .then(resp => {
            if (resp.data.status) {
                resolve();
            } else if (resp.status === 401) {
                firebase.auth().signOut()
                    .then(() => {
                        router.push('/auth');
                        store.dispatch('setUser', null);
                    });
            } else {
                if (process.env.NODE_ENV !== 'production') {
                    console.error(resp.data);
                }
                reject()
            }
        })
        .catch(err => {
            if (process.env.NODE_ENV !== 'production') {
                console.error(err);
            }

            if (err.response.status === 401) {
                logout();
            } else {
                reject();
            }
        });
});
