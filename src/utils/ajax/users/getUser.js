import firebase from 'firebase/app'

import axios from '@/plugins/axios'
import store from '@/store/store'
import logout from "../../logout";

export default uid => new Promise((resolve, reject) => {
    firebase.auth().currentUser.getIdToken(true)
        .then(idToken => {
            return axios({
                method: 'get',
                url: `/users/${uid}`,
                headers: {
                    Authorization: `Bearer ${idToken}`
                }
            });
        })
        .then(resp => {
            if (!!resp.data.status) {
                store.dispatch('setUser', resp.data.user);
                resolve();
            } else if (resp.data.status === 401) {
                firebase.auth().signOut()
                    .then(() => {
                        reject();
                    });
            } else {
                if (process.env.NODE_ENV !== 'production') {
                    console.error(resp.data);
                }
                reject();
            }
        })
        .catch(err => {
            if (process.env.NODE_ENV !== 'production') {
                console.error(err);
            }

            if (err.response.status === 401) {
                logout();
            } else {
                reject();
            }
        });
});
