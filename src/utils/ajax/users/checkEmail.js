import axios from '@/plugins/axios'
import logout from "../../logout";

export default data => new Promise((resolve, reject) => {
   axios.post('/users/check-email', data)
       .then(resp => {
           if (resp.data.status) {
               resolve(resp.data.exists);
           } else {
               reject(resp.data);
           }
       })
       .catch(err => {
           if (process.env.NODE_ENV !== 'production') {
               console.error(err);
           }

           if (err.response.status === 401) {
               logout();
           } else {
               reject();
           }
       });
});
