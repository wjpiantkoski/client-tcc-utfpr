const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

exports.addAdminClaim = functions.https.onCall((data, context) => {
    if (!!context.auth.token.admin) {
        return admin.auth().getUserByEmail(data.email)
            .then(user => admin.auth().setCustomUserClaims(user.uid, { admin: true }))
            .then(() => {
                return { status: true };
            })
            .catch(err => {
                return { status: false };
            });
    } else {
        return { status: false };
    }
});

exports.addPlaceClaim = functions.https.onCall((data, context) => {
    return admin.auth().getUserByEmail(data.email)
        .then(user => admin.auth().setCustomUserClaims(user.uid, { place: true }))
        .then(() => {
            return { status: true };
        })
        .catch(err => {
            return { status: false };
        });
});

exports.addPlayerClaim = functions.https.onCall((data, context) => {
    return admin.auth().getUserByEmail(data.email)
        .then(user => admin.auth().setCustomUserClaims(user.uid, { player: true }))
        .then(() => {
            return { status: true };
        })
        .catch(err => {
            return { status: false };
        });
});
